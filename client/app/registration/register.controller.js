// TODO: Send and retrieve information from server via Angular $http; modularize via services
// TODO: 6. Give controller access to EmpService services/functionalities
// TODO: 7. Use EmpService services to send information entered in view (HTML) to the server. Handle success/failure.
// TODO: 9. Show thank you page if registration is successful
// Always use an IIFE, i.e., (function() {})();
(function() {
    angular
        .module("EMS")          // to call an angular module, omit the second argument ([]) from the angular.module() syntax
        // this syntax is called the getter syntax
        .controller("RegCtrl", RegCtrl);    // angular.controller() attaches a controller to the angular module specified
                                            // as you can see, angular methods are chainable

    // TODO: 6.1 Inject the custom EmpService service so your controller can use its functionalities
    // TODO: 9.1 Inject the built-in service $window. This will be used to redirect to new page
    // Dependency injection. An empty [] means RegCtrl does not have dependencies. Here we inject EmpService so
    // RegCtrl can call services related to department.
    RegCtrl.$inject = ['$window', 'EmpService'];

    // TODO: 6.2 Accept the injected dependency as a parameter.
    // RegCtrl function declaration. A function declaration uses the syntax: functionName([arg [, arg [...]]]){ ... }
    // RegCtrl accepts the injected dependency as a parameter. We name it EmpService for consistency, but you may
    // assign any name
    function RegCtrl($window, EmpService) {

        // Declares the var vm (for ViewModel) and assigns it the object this (in this case, the RegCtrl)
        // Any function or variable that you attach to vm will be exposed to callers of RegCtrl, e.g., index.html
        var vm = this;
        var today = new Date();
        var birthday = new Date();
        birthday.setFullYear(birthday.getFullYear() - 18);

        // Exposed data models

        // Creates an employee object that
        // We expose the employee object by attaching it to the vm
        // This will allow us apply two-way data-binding to this object by using ng-model in our view (i.e., index.html)
        vm.employee = {
            empNo: "",
            firstname: "",
            lastname: "",
            gender: "male",
            birthday: birthday,
            hiredate: today,
            phonenumber: ""
        };

        // Exposed functions
        // Exposed functions can be called from the view. e.g., to call the vm.register from our view (index.html), code:
        // ctrl.register()
        vm.register = register;

        // Function declaration and definition
        function register() {

            // Calls alert box and displays registration information
            alert("The registration information you sent are \n" + JSON.stringify(vm.employee));

            // Prints registration information onto the client console
            console.log("The registration information you sent were:");
            console.log("Employee Number: " + vm.employee.empNo);
            console.log("Employee First Name: " + vm.employee.firstname);
            console.log("Employee Last Name: " + vm.employee.lastname);
            console.log("Employee Gender: " + vm.employee.gender);
            console.log("Employee Birthday: " + vm.employee.birthday);
            console.log("Employee Hire Date: " + vm.employee.hiredate);
            console.log("Employee Phone Number: " + vm.employee.phonenumber)

            // TODO: 7.1 In register function, pass the registration information to the appropriate EmpService
            // TODO: 7.1 function
            // TODO: 7.2 Handle promise object using .then() and .catch()
            // TODO: 9.2 On success, redirect to thanks.html
            // We call DeptService.insertDept to handle registration of department information. The data sent to this
            // function will eventually be inserted into the database.
            EmpService
                .insertEmp(vm.employee)
                .then(function (result) {
                    console.log("result " + JSON.stringify(result));
                    $window.location.assign('/app/registration/thanks.html');
                })
                .catch(function (err) {
                    console.log("error " + err);
                });

        } // END function register()

    } // END RegCtrl



})();