// TODO: Send and retrieve information from server via Angular $http; modularize via services
// TODO: 1. Define a service that would hold logic related to employee
// TODO: 2. Inject the dependency needed to communicate with the server
// TODO: 3. Create a function that accepts registration information and sends this info to server.
// TODO: 3a Name the function insertEmp
// TODO: 3b Return promise object to calling controller
// TODO: 3c Expose this function.
// TODO: 11 Create a function called getEmpAll that retrieves all employee information from server using HTTP GET.
// TODO: 11 This function returns a promise object.


// TODO: 1.1 Create an IIFE then call your angular module and attach your service to it. Name the service EmpService
// Always use an IIFE, i.e., (function() {})();
(function () {
    // Attaches EmpService service to the DMS module
    angular
        .module("EMS")
        .service("EmpService", EmpService);

    // TODO: 2.1 Inject $http. We will use this built-in service to communicate with the server
    // Dependency injection. Here we inject $http because we need this built-in service to communicate with the server
    // There are different ways to inject dependencies; $inject is minification safe
    EmpService.$inject = ['$http'];

    // TODO: 1.2 Declare EmpService
    // TODO: 2.2 Accept the injected dependency as a parameter.
    // EmpService function declaration
    // Accepts the injected dependency as a parameter. We name it $http for consistency, but you may assign any name
    function EmpService($http) {

        // TODO: 1.3 Assign this object to a variable named service
        // Declares the var service and assigns it the object this (in this case, the EmpService). Any function or
        // variable that you attach to service will be exposed to callers of EmpService, e.g., search.controller.js
        // and register.controller.js
        var service = this;

        // Exposed data models -----------------------------------------------------------------------------------------
        // Exposed functions -------------------------------------------------------------------------------------------
        // TODO: 3.3 Expose insertEmp
        service.insertEmp = insertEmp;
        // TODO: 11.2 Expose retrieveEmp. Arrange in alphabetical order
        service.retrieveEmp = retrieveEmp;

        // Function declaration and definition -------------------------------------------------------------------------

        // TODO: 3.1 Declare insertEmp; insertEmp must be a private function; insertEmp accepts employee
        // TODO: 3.1  information and returns a promise object to the calling function
        // insertEmp uses HTTP POST to send employee information to the server's /employees route
        // Parameters: employee information; Returns: Promise object
        function insertEmp(employee) {
            // TODO: 3.2 Return the $http promise object to calling function
            // This line returns the $http to the calling function
            // This configuration specifies that $http must send the employee data received from the calling function
            // to the /employees route using the HTTP POST method. $http returns a promise object. In this instance
            // the promise object is returned to the calling function
            return $http({
                method: 'POST'
                , url: 'employees'
                , data: {emp: employee}
            });
        }


        // TODO: 11.1 Declare retrieveEmp; retrieveEmp must be a private function; retrieveEmp retrieves all emp data
        // TODO: 11.1 from server and returns a promise object
        // retrieveEmp retrieves employee information from the server via HTTP GET.
        // Parameters: none. Returns: Promise object
        function retrieveEmp(){
            return $http({
                method: 'GET'
                , url: 'employees'
            });
        }

    }
})();