// TODO: Send and retrieve information from server via Angular $http
// TODO: 4. Update handling of /register to match client-side changes
// TODO: 5. In register (employee) handler, return a status instead of a thank you page.
// TODO: 10. Define route that handles GET /employees (request from getAllEmp)
// TODO: 10a. Move vm.employees content from search.controller.js to here

// Loads express module and assigns it to a var called express
var express = require("express");

// Loads path to access helper functions for working with files and directory paths
var path = require("path");

// Loads bodyParser to populate and parse the body property of the request object
var bodyParser = require("body-parser");

//Create an instance of express application
var app = express();

// Defines server port.
// Value of NODE_PORT is taken from the user environment if defined; port 3000 is used otherwise.
const NODE_PORT = process.env.NODE_PORT || 3000;

// Defines paths
// __dirname is a global that holds the directory name of the current module
const CLIENT_FOLDER = path.join(__dirname + '/../client');
const MSG_FOLDER = path.join(CLIENT_FOLDER + '/assets/messages');

// Serves files from public directory (in this case CLIENT_FOLDER).
// __dirname is the absolute path of the application directory.
// if you have not defined a handler for "/" before this line, server will look for index.html in CLIENT_FOLDER
app.use(express.static(CLIENT_FOLDER));

// TODO: 4.1 Update the body parser type to match the data format sent through $http
// TODO: 4.1a Instead of deleting bodyParser.urlencoded comment it out so you could refer to in the future
// Populates req.body with information submitted through the registration form.
// Expected content type is application/x-www-form-urlencoded
//app.use(bodyParser.urlencoded({extended: false}));
// Default $http content type is application/json so we use json as the parser type
app.use(bodyParser.json());

// TODO: 4.2 Update register route to route used by client app
// Defines endpoint exposed to client side for registration
app.post("/employees", function(req, res, next) {
    // Information sent via an HTTP POST is found in req.body
    console.log('\nInformation submitted to server:')
    console.log('Employee No: ' + req.body.empNo);
    console.log('Firstname: ' +req.body.firstname);
    console.log('Lastname: ' +  req.body.lastname);
    console.log('Gender: ' + req.body.gender);
    console.log('Birthday: ' + req.body.birthday);
    console.log('Hire Date: ' + req.body.hiredate);

    // res.send responds with a thank you page to client. This property also ends the req/res cycle.
    // res.status sets the status code sent to client.
    res.status(200).end();
});

// TODO: 10.1 Create the HTTP GET /employees handler
// Defines endpoint exposed to client side for retrieving all employee information
app.get("/employees", function (req, res) {
    var employees = [
        {
            empNo: 1001,
            empFirstName: 'Emily',
            empLastName: 'Smith',
            empPhoneNumber: '6516 2093'

        }
        , {
            empNo: 1002,
            empFirstName: 'Varsha',
            empLastName: 'Jansen',
            empPhoneNumber: '6516 2093'
        }
        ,
        {
            empNo: 1003,
            empFirstName: 'Julie',
            empLastName: 'Black',
            empPhoneNumber: '6516 2093'
        }
        , {
            empNo: 1004,
            empFirstName: 'Fara',
            empLastName: 'Johnson',
            empPhoneNumber: '6516 2093'
        }
        ,
        {
            empNo: 1005,
            empFirstName: 'Justin',
            empLastName: 'Zhang',
            empPhoneNumber: '6516 2093'
        }
        , {
            empNo: 1006,
            empFirstName: 'Kenneth',
            empLastName: 'Black',
            empPhoneNumber: '6516 2093'
        }

    ];
    // TODO: 10.3 Return the employees information as a json object
    // Return employees as a json object
    res.json(200, employees);
});

// Handles 404. In Express, 404 responses are not the result of an error,
// so the error-handler middleware will not capture them.
// To handle a 404 response, add a middleware function at the very bottom of the stack
// (below all other path handlers)
app.use(function (req, res) {
    res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});

// Error handler: server error
app.use(function (err, req, res, next) {
    res.status(501).sendFile(path.join(MSG_FOLDER + '/501.html'));
});

// Server starts and listens on NODE_PORT
app.listen(NODE_PORT, function () {
    console.log("Server running at http://localhost:" + NODE_PORT);
});